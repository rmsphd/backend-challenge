FROM openjdk:8-jdk-alpine
EXPOSE 7000
ADD /build/libs/backend-challenge-1.0-SNAPSHOT.jar backend-challenge.jar
ENTRYPOINT ["java", "-jar", "backend-challenge.jar"]
