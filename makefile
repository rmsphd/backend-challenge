jar:
	./gradlew build
	docker build . -t backend-challenge
run-docker-image:
	docker run -p 7000:7000 -it backend-challenge
run:
	./gradlew run
test:
	./gradlew clean test
check:
	./gradlew clean check
run-docker:
	docker run --rm -p 7000:7000 -u gradle -v "$(PWD)":/home/gradle/project -w /home/gradle/project gradle:5.2.1-jdk8-alpine gradle clean run
test-docker:
	docker run --rm -u gradle -v "$(PWD)":/home/gradle/project -w /home/gradle/project gradle:5.2.1-jdk8-alpine gradle clean test
check-docker:
	docker run --rm -u gradle -v "$(PWD)":/home/gradle/project -w /home/gradle/project gradle:5.2.1-jdk8-alpine gradle clean check
