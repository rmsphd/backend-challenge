package br.com.sienna.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class PasswordStrengthTest {

    @TestFactory
    fun isValid():Collection<DynamicTest> = listOf(
        "Abc123de@" to true,
        "Abc123@ae" to true,
        "Abc123dc@" to false, // hasUniqueCharacters=false
        "abc123de@" to false, // hasUpper=false
        "Abc123de4" to false, // hasSpecial=false
        "ABC123DE@" to false, // hasLower=false
        "Abcdefghij@" to false, // hasDigit=false
        "Abc123@d" to false // hasMinimumLength=false
        ).map { (input, expected) ->
            DynamicTest.dynamicTest("when call isValid with valid password '$input' then I get '$expected'") {
                assertEquals(expected, PasswordStrength().isValid(PasswordRequest(input)).valid)
            }
        }
}
