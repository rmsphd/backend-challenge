package br.com.sienna.http

import br.com.sienna.config.Config
import io.javalin.Javalin
import kong.unirest.HttpResponse
import kong.unirest.JsonNode
import kong.unirest.Unirest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ServerTest {

    private lateinit var app:Javalin
    private val serverUrl = "http://localhost:${Config.serverPort}"

    @BeforeAll
    fun setUp() {
        app = Server().createApp()
    }

    @AfterAll
    fun tearDown() {
        app.stop()
    }


    @Nested
    inner class `POST password` {

        @Test
        fun `POST valid password should be return valid equals true`() {
            val respose: HttpResponse<JsonNode> = Unirest.post("${serverUrl}/password")
                .body("{\"password\":\"Abc123@ed\"}")
                .asJson()

            assertEquals(200, respose.status)
            assertEquals(true, respose.body.`object`["valid"])
        }

        @Test
        fun `POST invalid password should be return valid equals false`() {
            val respose: HttpResponse<JsonNode> = Unirest.post("${serverUrl}/password")
                .body("{\"password\":\"Abc123@bd\"}")
                .asJson()

            assertEquals(200, respose.status)
            assertEquals(false, respose.body.`object`["valid"])
        }

        @Test
        fun `POST invalid entity should be return Unprocessable Entity`() {
            val respose: HttpResponse<JsonNode> = Unirest.post("${serverUrl}/password")
                .body("{\"text\":\"Abc123@bd\"}")
                .asJson()

            assertEquals(422, respose.status)
        }

    }

    @Nested
    inner class `GET health` {
        @Test
        fun `GET health should be return success`() {
            val response: HttpResponse<String> = Unirest.get("${serverUrl}/health").asString()

            assertEquals(200, response.status)
            assertEquals("OK", response.body)
        }
    }

}
