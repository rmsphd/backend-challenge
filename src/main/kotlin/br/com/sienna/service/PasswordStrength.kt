package br.com.sienna.service

class PasswordStrength {

    companion object {
        private const val REQUIRED_LENGTH = 9 // Nove ou mais caracteres
    }

    fun isValid(request: PasswordRequest): PasswordResponse {
        val hasMinimumLength = request.password.length >= REQUIRED_LENGTH
        var hasDigit = false
        var hasLower = false
        var hasUpper = false
        var hasSpecial = false
        var hasUniqueCharacters = true
        val characters = HashSet<Char>()

        for (i in 0 until request.password.length) {
            val c = request.password[i]

            if (characters.contains(c)) {
                hasUniqueCharacters = false // Não possuir caracteres repetidos
            }
            characters.add(c)

            when {
                !Character.isLetterOrDigit(c) -> hasSpecial = true // Ao menos 1 caractere especial
                Character.isDigit(c) -> hasDigit = true // Ao menos 1 dígito
                Character.isUpperCase(c) -> hasUpper = true // Ao menos 1 letra maiúscula
                else -> hasLower = true // Ao menos 1 letra minúscula
            }

        }

        return PasswordResponse(hasMinimumLength && hasDigit && hasLower
                && hasUpper && hasSpecial && hasUniqueCharacters)
    }

}

data class PasswordResponse(val valid: Boolean)
data class PasswordRequest(val password: String)
