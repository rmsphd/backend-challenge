package br.com.sienna.http

import br.com.sienna.config.Config
import io.javalin.plugin.openapi.InitialConfigurationCreator
import io.javalin.plugin.openapi.OpenApiOptions
import io.javalin.plugin.openapi.ui.SwaggerOptions
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.servers.Server

object SwaggerHandler {

    fun getOpenApiOptions(): OpenApiOptions {
        val initialConfigurationCreator = InitialConfigurationCreator {
            OpenAPI()
                .info(Info().version("1.0").description("Password Strength Application"))
                .addServersItem(Server().url("http://localhost:${Config.serverPort}").description("localhost"))
        }

        return OpenApiOptions(initialConfigurationCreator)
            .path("/swagger-docs")
            .swagger(SwaggerOptions("/swagger").title("My Swagger Documentation"))
    }
}
