package br.com.sienna.http

import io.javalin.Javalin
import io.javalin.http.BadRequestResponse
import org.eclipse.jetty.http.HttpStatus


object ExceptionHandler {

    fun registerHandlers(app: Javalin) {
        app.exception(BadRequestResponse::class.java) { _, ctx ->
            ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422)
        }
    }
}
