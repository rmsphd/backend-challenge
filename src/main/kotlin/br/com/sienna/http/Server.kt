package br.com.sienna.http

import br.com.sienna.config.Config
import br.com.sienna.service.PasswordStrength
import br.com.sienna.service.PasswordRequest
import br.com.sienna.service.PasswordResponse
import io.javalin.Javalin
import io.javalin.plugin.openapi.OpenApiPlugin
import io.javalin.plugin.openapi.dsl.document
import io.javalin.plugin.openapi.dsl.documented
import org.eclipse.jetty.http.HttpStatus


class Server {

    private val passwordStrength = PasswordStrength()

    fun createApp(): Javalin {
        val app = Javalin.create { config ->
            config.registerPlugin(OpenApiPlugin(SwaggerHandler.getOpenApiOptions()))
        }.start(Config.serverPort)

        ExceptionHandler.registerHandlers(app)

        val healthDocumentation = document()
            .operation { it.summary("Health check") }

        app.get("/health", documented(healthDocumentation) { ctx -> ctx.result("OK") })

        val passwordStrengthDocumentation = document()
            .operation { it.summary("Verify password Strength") }
            .body(PasswordRequest::class.java)
            .json(HttpStatus.OK_200.toString(), PasswordResponse::class.java)

        app.post("/password", documented(passwordStrengthDocumentation) { ctx ->
            ctx.json(passwordStrength.isValid(ctx.bodyAsClass(PasswordRequest::class.java)))
        })

        return app
    }

}
