# Backend Challenge

- Health Check [http://localhost:7000/health](http://localhost:7000/health)
- OpenAPI [http://localhost:7000/swagger](http://localhost:7000/swagger)
- Exemplo de chamada para endpoint que verifica a senha.
```
curl --location --request POST 'http://localhost:7000/password' \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "password": "123Abc@da"
     }'
```

## Dependências

- Java >= 1.8 ou 
- Docker

## Desenvolvimento

### Iniciar a aplicação
- make run ou
- make run-docker

### Executar os testes
- make test ou
- make test-docker

### Executar a verificação estática de código
- make check ou
- make check-docker

## Exemplo de imagem para deploy
- make jar
- make run-docker-image

## Decisões técnicas
- Kotlin porque é uma Stack que gosto e já trabalhei antes, apesar de não ser a Stack que tenho mais experiência.
- Javalin com Jetty, por causa da simplicidade e desempenho, permite criar um serviço REST sem grandes dependências, start rápido, baixo footprint.
- Konfig, biblioteca independente que permite facilmente controlar configurações estáticas.
- Testes unitários e de integração com Junit5 e Unirest para client Http.
- Documentação da API usando OpenAPI 3/Swaager integrada ao código, sempre que codificamos um novo endpoint 
já documentamos.
- Análise estática de código com Detekt.
- Cobertura de código com Jacoco
- Docker para desenvolvimento e para deploy, agiliza o setup de um novo colaborador e padroniza para rodar a mesma aplicação local e em ambiente produtivo.
- Sem injeção de dependência, pois, é um projeto muito pequeno e não tem necessidade.

## Próximos passos
- Melhorar o tratamento de exceção quando ocorre 422 UNPROCESSABLE ENTITY.
- Adicionar log na aplicação.
- Adicionar trace distribuído, como Opentrace, Datadog ou Kibana.
- Configurar os helms para deploy em kubernetes ou até este é um caso que funciona muito bem com Lambda Functions.
